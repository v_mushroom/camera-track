/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.cameratrack;

import java.awt.Image;
import java.awt.image.BufferedImage;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.Java2DFrameConverter;

/**
 *
 * @author mush
 */
public class CameraImageGrabber {

    private FrameGrabber grabber;
    private Java2DFrameConverter jConverter;

    public CameraImageGrabber() throws Exception {
        System.out.println("FrameGrabber create ...");
        grabber = FrameGrabber.create(FrameGrabber.get("OpenCVFrameGrabber"), int.class, 0);
        
        grabber.setImageMode(FrameGrabber.ImageMode.COLOR);
        grabber.setImageWidth(160);
        grabber.setImageHeight(120);
        
        System.out.println("FrameGrabber start ...");
        grabber.start();
        System.out.println("FrameGrabber started");

        // CanvasFrame, FrameGrabber, and FrameRecorder use Frame objects to communicate image data.
        // We need a FrameConverter to interface with other APIs (Android, Java 2D, JavaFX, Tesseract, OpenCV, etc).
        jConverter = new Java2DFrameConverter();
    }

    public BufferedImage grab() {
        try {
            Image aImage = jConverter.convert(grabber.grab());
            if (aImage instanceof BufferedImage) {
                return (BufferedImage) aImage;
            }
        } catch (FrameGrabber.Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public void stop() {
        try {
            System.out.println("FrameGrabber stop ...");
            grabber.stop();
        } catch (FrameGrabber.Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
