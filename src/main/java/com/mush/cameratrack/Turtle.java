/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.cameratrack;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class Turtle {

    public Point2D.Double position;
    public double rotation;

    public Turtle() {
        position = new Point2D.Double();
        rotation = 0;
    }

    public void draw(Graphics2D g, int width, int height) {
        AffineTransform t = g.getTransform();
        int halfWidth = width / 2;
        int halfHeight = height / 2;

        double x = (1 + position.x) * halfWidth;
        double y = (1 + position.y) * halfHeight;

        x = x > 0 ? x % width : (width - (-x % width));
        y = y > 0 ? y % height : (height - (-y % height));

        g.translate(x, y);
        g.rotate(rotation);

        g.setColor(Color.WHITE);
        g.fillRect(-5, -5, 10, 10);
        g.setColor(Color.RED);
        g.drawRect(-5, -5, 10, 10);
        g.drawLine(0, -5, 0, 0);

        g.setTransform(t);
    }

    public void move(double forward, double turn) {
        double sin = Math.sin(rotation - Math.PI / 2);
        double cos = Math.cos(rotation - Math.PI / 2);
        position.x += forward * cos;
        position.y += forward * sin;
        rotation += turn;
    }

}
