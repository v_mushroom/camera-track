/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.cameratrack;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author mush
 */
public class PatchFinder {

    public ColorFinder colorFinder = new ColorFinder();

    public Rectangle2D findPatch(BufferedImage image) {
        if (image == null) {
            return null;
        }

        Point center = findCenter(image);
        if (center == null) {
            return null;
        }

//        Rectangle first = findBoxAroundCenter(image, center);
//        center.x = first.x + first.width / 2;
//        center.y = first.y + first.height / 2;

        Rectangle second = findBoxAroundCenter(image, center);

        return normalize(second, image);
    }

    public static float getPatchCenterHue(Rectangle2D patch, BufferedImage image) {
        if (patch == null) {
            return 0;
        }
        return getPixelHueAtUv(patch.getCenterX(), patch.getCenterY(), image);
    }

    public static float getPixelHueAtUv(double u, double v, BufferedImage image) {
        return getPixelHsvAtUv(u, v, image)[0];
    }

    public static float[] getPixelHsvAtUv(double u, double v, BufferedImage image) {
        int x = (int) (u * image.getWidth());
        int y = (int) (v * image.getHeight());

        return ColorFinder.getPixelHsv(x, y, image);
    }

    private Rectangle2D normalize(Rectangle patch, BufferedImage image) {
        return new Rectangle2D.Double(
                (double) patch.x / image.getWidth(),
                (double) patch.y / image.getHeight(),
                (double) patch.width / image.getWidth(),
                (double) patch.height / image.getHeight()
        );
    }

    private Rectangle findBoxAroundCenter(BufferedImage image, Point center) {
        int left = center.x - 1;
        int right = center.x + 1;
        int top = center.y - 1;
        int bottom = center.y + 1;

        boolean hasLeft = true;
        boolean hasRight = true;
        boolean hasTop = true;
        boolean hasBottom = true;

        while (hasLeft || hasRight || hasTop || hasBottom) {
            int width = right - left;
            int height = bottom - top;

            if (hasLeft) {
                left--;
            }
            if (hasRight) {
                right++;
            }
            if (hasTop) {
                top--;
            }
            if (hasBottom) {
                bottom++;
            }
            hasLeft = testLine(image, left, top, 0, 1, height);
            hasRight = testLine(image, right, top, 0, 1, height);
            hasTop = testLine(image, left, top, 1, 0, width);
            hasBottom = testLine(image, left, bottom, 1, 0, width);
        }

        return new Rectangle(left, top, right - left, bottom - top);
    }

    private boolean testLine(BufferedImage image, int x0, int y0, int dx, int dy, int length) {
        // is there any matching pixel on this line?
        int x = x0;
        int y = y0;
        for (int i = 0; i < length; i++) {
            if (colorFinder.matchesTarget(x, y, image)) {
                return true;
            }
            x += dx;
            y += dy;
        }
        return false;
    }

    private Rectangle findBoxAroundCenter_(BufferedImage image, Point center) {
        int left = center.x;
        int right = center.x;
        int top = center.y;
        int bottom = center.y;

        Point leftEnd = findEnd(image, center.x, center.y, -1, 0);
        Point rightEnd = findEnd(image, center.x, center.y, 1, 0);
        Point topEnd = findEnd(image, center.x, center.y, 0, -1);
        Point bottomEnd = findEnd(image, center.x, center.y, 0, 1);

        left = Math.min(leftEnd.x, left);
        right = Math.max(rightEnd.x, right);
        top = Math.min(topEnd.y, top);
        bottom = Math.max(bottomEnd.y, bottom);

        Point topLeftEnd = findEnd(image, center.x, center.y, -1, -1);
        Point topRightEnd = findEnd(image, center.x, center.y, 1, -1);
        Point bottomRightEnd = findEnd(image, center.x, center.y, 1, 1);
        Point bottomLeftEnd = findEnd(image, center.x, center.y, -1, 1);

        left = Math.min(topLeftEnd.x, left);
        left = Math.min(bottomLeftEnd.x, left);
        right = Math.max(topRightEnd.x, right);
        right = Math.max(bottomRightEnd.x, right);
        top = Math.min(topLeftEnd.y, top);
        top = Math.min(topRightEnd.y, top);
        bottom = Math.max(bottomLeftEnd.y, bottom);
        bottom = Math.max(bottomRightEnd.y, bottom);

        return new Rectangle(left, top, right - left, bottom - top);
    }

    private Point findEnd(BufferedImage image, int x0, int y0, int dx, int dy) {
        int x = x0;
        int y = y0;
        while (colorFinder.matchesTarget(x, y, image)) {
            x += dx;
            y += dy;
        }
        return new Point(x, y);
    }

    private Point findCenter(BufferedImage image) {
        boolean found = false;
        int bestX = 0;
        int bestY = 0;
        float bestIntensity = 0;

        for (int y = 5; y < image.getHeight(); y += 5) {
            int xOfs = 5 + 5 * ((y % 10) / 5);
            for (int x = xOfs; x < image.getWidth(); x += 10) {
                float intensity = colorFinder.getMatchIntensity(x, y, image);

                if (intensity > bestIntensity) {
                    found = true;
                    bestX = x;
                    bestY = y;
                    bestIntensity = intensity;
                }
            }
        }
        if (found) {
            return new Point(bestX, bestY);
        } else {
            return null;
        }
    }

}
