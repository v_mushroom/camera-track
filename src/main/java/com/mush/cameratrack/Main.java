/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.cameratrack;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import org.bytedeco.javacv.CanvasFrame;

/**
 *
 * @author mush
 */
public class Main {

    PatchFinder finder;
    PatchTracker tracker;
    SequenceAnalyzer analyzer;
    Turtle turtle;

    public Main() {
        finder = new PatchFinder();
        tracker = new PatchTracker();
        analyzer = new SequenceAnalyzer(20);
        turtle = new Turtle();
        finder.colorFinder.targetHsv[0] = 0.55f;
        finder.colorFinder.targetHsv[1] = 0.95f;
        finder.colorFinder.targetHsv[2] = 0.95f;
    }

    public static void main(String[] args) throws Exception {
        Main main = new Main();

        CameraImageGrabber grabber = new CameraImageGrabber();

        final CanvasFrame frame = new CanvasFrame("Some Title");
        frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                grabber.stop();
            }
        });

        while (frame.isVisible()) {
            BufferedImage image = grabber.grab();
            if (image != null) {
                main.process(image);
                frame.showImage(image);
            }
        }

        frame.dispose();
    }

    public void process(BufferedImage image) {
        Rectangle2D patch = finder.findPatch(image);
        tracker.track(patch);
        if (patch != null) {
            analyzer.add(patch.getMinY());
        }

//        System.out.println(analyzer.frequency);
        double frequency = analyzer.frequency;
        if (frequency < 0.05 || analyzer.amplitude < 0.02) {
            frequency = 0;
        }
        double speed = frequency * tracker.getConfidence() * analyzer.confidence * 0.1;
        double turn = speed * (tracker.getCurrent().getCenterX() - 0.5) * 10;
        turtle.move(speed, -turn);

        draw(image, patch);
    }

    public void draw(BufferedImage image, Rectangle2D patch) {

        int x0 = image.getWidth() / 2;
        int y0 = image.getHeight() / 2;
        Color hueColor = ColorFinder.getHueColor(finder.colorFinder.targetHsv);
        Color centerColor = ColorFinder.getHueColor(finder.getPatchCenterHue(patch, image));

        Object data = image.getRaster().getDataElements(x0, y0, null);
        int red = image.getColorModel().getRed(data);
        int green = image.getColorModel().getGreen(data);
        int blue = image.getColorModel().getBlue(data);

        Graphics2D g = (Graphics2D) image.getGraphics();

//        g.setColor(Color.BLACK);
//        for (int i = 0; i < image.getHeight(); i += 10) {
//            g.fillRect(0, i, image.getWidth(), 10);
//        }
//        g.setColor(Color.YELLOW);
//        for (int v = 5; v < image.getHeight(); v += 5) {
//            int uOfs = 5 + 5 * ((v % 10) / 5);
//            for (int u = uOfs; u < image.getWidth(); u += 10) {
//                g.drawRect(u, v, 0, 0);
//            }
//        }
        g.setColor(Color.RED);
//        g.drawRect(x0, y0, 1, 1);
//        g.drawString("" + red + " " + green + " " + blue, x0, y0);

        final float[] hsv = new float[3];
        Color.RGBtoHSB(red, green, blue, hsv);

        g.setColor(Color.GRAY);
        g.drawLine(x0 - 50, y0 - 55, x0 - 50, y0 - 40);
        g.drawLine(x0 + 50, y0 - 55, x0 + 50, y0 - 40);
        g.setColor(Color.RED);
        g.drawLine((int) (x0 - 50 + hsv[0] * 100), y0 - 55, (int) (x0 - 50 + hsv[0] * 100), y0 - 50);
        g.drawLine((int) (x0 - 50 + hsv[1] * 100), y0 - 50, (int) (x0 - 50 + hsv[1] * 100), y0 - 45);
        g.drawLine((int) (x0 - 50 + hsv[2] * 100), y0 - 45, (int) (x0 - 50 + hsv[2] * 100), y0 - 40);

        g.setColor(Color.GRAY);
        g.drawRect(x0 - 60, y0 - 20, 10, 10);
        g.setColor(Color.RED);
        g.drawRect((int) (x0 - 60 + hsv[0] * 10), (int) (y0 - 20 + hsv[1] * 10), 0, 0);
        g.setColor(Color.GREEN);
        g.drawRect((int) (x0 - 60 + tracker.getConfidence() * 10), y0 - 22, 0, 0);

        g.setColor(Color.MAGENTA);
        g.drawRect(
                (int) (tracker.getCurrent().getX() * image.getWidth()),
                (int) (tracker.getCurrent().getY() * image.getHeight()),
                (int) (tracker.getCurrent().getWidth() * image.getWidth()),
                (int) (tracker.getCurrent().getHeight() * image.getHeight()));

        Color c = new Color(red, green, blue);
        g.setColor(c);
        g.fillRect(x0, y0 + 20, 10, 10);

        g.setColor(hueColor);
        g.fillRect(x0, y0 + 31, 10, 10);
        g.setColor(centerColor);
        g.fillRect(x0, y0 + 43, 10, 10);

        g.setColor(Color.MAGENTA);
        for (int i = 0; i < analyzer.history.length; i++) {
            g.drawRect(x0 + 20 + i, 10 + (int) (analyzer.history.get(i) * 100), 0, 0);
        }
        g.setColor(Color.BLUE);
        for (int i = 0; i < analyzer.history.length; i++) {
            g.drawRect(x0 + 20 + i, 10 + (int) (analyzer.average.get(i) * 100), 0, 0);
        }
        g.drawRect(x0 + 45, 10, 0, (int) (analyzer.amplitude * 100));
        g.setColor(Color.GRAY);
        g.drawRect(x0 + 50, y0, analyzer.autoCorrelation.length, 0);
        g.setColor(Color.YELLOW);
        g.drawRect(x0 + 50, y0, (int) (analyzer.autoCorrelation.length * analyzer.frequency), 0);
        g.setColor(Color.RED);
        int minIndex = -1;
        double minAutoCorrelation = 0;
        for (int i = 0; i < analyzer.autoCorrelation.length; i++) {
            double autoCorrelation = analyzer.autoCorrelation.get(i);
//            if (analyzer.amplitude != 0) {
//                //autoCorrelation /= analyzer.amplitude;
//            }
            g.drawRect(x0 + 50 + i, y0 + (int) (autoCorrelation * 10), 0, 0);
            if (autoCorrelation < minAutoCorrelation) {
                minIndex = i;
                minAutoCorrelation = autoCorrelation;
            }

        }
        if (minIndex >= 0) {
            g.setColor(Color.YELLOW);
            g.drawRect(x0 + 50 + minIndex, y0 - (int) (analyzer.confidence * 10), 0, (int) (analyzer.confidence * 10));
        }

        if (patch != null) {
            g.setColor(Color.GREEN);
            g.drawRect(
                    (int) (patch.getX() * image.getWidth()),
                    (int) (patch.getY() * image.getHeight()),
                    (int) (patch.getWidth() * image.getWidth()),
                    (int) (patch.getHeight() * image.getHeight()));
        }

        turtle.draw(g, image.getWidth(), image.getHeight());

        g.dispose();
    }

}
