/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.cameratrack;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author mush
 */
public class ColorFinder {

    public float[] targetHsv = new float[]{0.05f, 0.5f, 0.8f};

    public float totalTreshold = 0.5f;
    public float hueTreshold = 0.75f;
    public float satTreshold = 0.5f;
    public float valTreshold = 0.5f;

    public void drawReferenceImage(BufferedImage cameraImage, int step, Graphics2D g) {
        int width = cameraImage.getWidth() / step;
        int height = cameraImage.getHeight() / step;

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                float[] hsv = getPixelHsv(x * step, y * step, cameraImage);

                Color color;
                if (targetHsv != null) {
                    color = getMatchIntensityWithTresholdColor(hsv);
                } else {
                    color = getHueColor(hsv);
                }
                g.setColor(color);
                g.fillRect(x, y, 1, 1);
            }
        }
    }

    public static Color getHueColor(float[] hsv) {
        return new Color(Color.HSBtoRGB(hsv[0], 1, 1));
    }

    public static Color getHueColor(float hue) {
        return new Color(Color.HSBtoRGB(hue, 1, 1));
    }

    public static float[] getPixelHsv(int x, int y, BufferedImage image) {
        if (x < 0 || x >= image.getWidth() || y < 0 || y >= image.getHeight()) {
            return null;
        }

        Object data = image.getRaster().getDataElements(x, y, null);

        int red = image.getColorModel().getRed(data);
        int green = image.getColorModel().getGreen(data);
        int blue = image.getColorModel().getBlue(data);

        final float[] hsv = new float[3];
        Color.RGBtoHSB(red, green, blue, hsv);
        return hsv;
    }

    private Color intensityColor(float f) {
        return new Color(f, f, f);
    }

    private float treshold(float v, double min) {
        return v > min ? 1 : 0;
    }

    private float tresholdScale(float v, float min) {
        // v : 0 .. 1
        // (v - min) : 0 .. 1 - min
        // (v - min) / (1 - min) : 0 .. 1
        return v < min || min >= 1
                ? 0
                : (v - min) / (1 - min);
    }

    public boolean matchesTarget(int x, int y, BufferedImage image) {
        float[] hsv = getPixelHsv(x, y, image);
        return hsv == null ? false : matchesTarget(hsv);
    }

    public boolean matchesTarget(float[] hsv) {
        return distance(hsv, targetHsv) > totalTreshold;
    }

    public float getMatchIntensity(int x, int y, BufferedImage image) {
        float[] hsv = getPixelHsv(x, y, image);
        return hsv == null ? 0 : distance(hsv, targetHsv);
    }

    public Color getMatchIntensityWithTresholdColor(float[] hsv) {
        float distance = distance(hsv, targetHsv);
        return intensityColor(distance > totalTreshold ? 1 : distance);
    }

    public static float getHueOffset(float hueA, float hueB) {
        float offset = Math.abs(hueA - hueB);
        if (offset > 0.5) {
            offset = 1 - offset;
        }
        return offset;
    }

    public float distance(float[] hsv, float[] target) {
        float hueF = 1 - getHueOffset(hsv[0], target[0]);
        float satF = 1 - Math.abs(hsv[1] - target[1]);
        float valF = 1 - Math.abs(hsv[2] - target[2]);

        hueF = tresholdScale(hueF, hueTreshold);
        satF = tresholdScale(satF, satTreshold);
        valF = tresholdScale(valF, valTreshold);

        return hueF * satF * valF;
    }

}
