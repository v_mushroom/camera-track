/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.cameratrack;

/**
 *
 * @author mush
 */
public class SequenceAnalyzer {

//    public double[] history;
//    public double[] average;
//    public double[] autoCorrelation;
//    private int position;
    public Sequence history;
    public Sequence average;
    public Sequence autoCorrelation;
    public double amplitude;
    public double frequency;
    public double confidence;

    public SequenceAnalyzer(int len) {
//        history = new double[len];
//        average = new double[len];
//        autoCorrelation = new double[len];
        history = new Sequence(len);
        average = new Sequence(len);
        autoCorrelation = new Sequence(len);
        clear();
    }

    public void clear() {
//        for (int i = 0; i < history.length; i++) {
//            history[i] = 0;
//            average[i] = 0;
//            autoCorrelation[i] = 0;
//        }
//        position = 0;
        history.clear();
        average.clear();
        autoCorrelation.clear();
        confidence = 0;
        frequency = 0;
        amplitude = 0;
    }

    public void add(double value) {
//        history[position] = value;
//        average[position] = average();
        history.add(value);
        average.add(history.average());

        amplitude();
        autoCorrelate();

//        position = index(position + 1);
    }

    public void amplitude() {
        double max = 0;
        for (int i = 0; i < history.length; i++) {
//            double abs = Math.abs(history[i] - average[i]);
            double abs = Math.abs(history.get(i) - average.get(i));
            if (abs > max) {
                max = abs;
            }
        }
        amplitude = max;
    }

    public void autoCorrelate() {
        int minIndex = -1;
        double min = 0;
        double max = 0;
        boolean beenUnderZero = false;
        boolean foundMin = false;
        for (int i = 0; i < autoCorrelation.length && !foundMin; i++) {
//            autoCorrelation[i] = autoCorrelate(i);
            autoCorrelation.set(i, autoCorrelate(i));
            if (i == 0) {
//                max = autoCorrelation[i];
//                autoCorrelation[i] = 1;
                max = autoCorrelation.get(i);
                autoCorrelation.set(i, 1);
            } else if (max != 0) {
//                autoCorrelation[i] /= max;
                autoCorrelation.set(i, autoCorrelation.get(i) / max);
            }
//            if (autoCorrelation[i] < 0) {
            if (autoCorrelation.get(i) < 0) {
                beenUnderZero = true;
            } else if (beenUnderZero) {
                foundMin = true;
            }
//            if (autoCorrelation[i] < min) {
            if (autoCorrelation.get(i) < min) {
                minIndex = i;
//                min = autoCorrelation[i];
                min = autoCorrelation.get(i);
            }
        }
        if (minIndex > 1) {
            // need to divide by 2 to get per second
            frequency = frequency * 0.9 + 0.1 * (0.5 / minIndex);
            confidence = 1;
        } else {
            frequency = 0;
            confidence = 0;
        }
    }

    public double autoCorrelate(int lag) {
        double sum = 0;
        int len = history.length - lag;
        for (int i = 0; i < len; i++) {
//            int n = index(position - i);
//            int nl = index(position - i - lag);
//            double yn = history[n] - average[n];
//            double ynl = history[nl] - average[nl];

            int n = history.index(history.getPosition() - i);
            int nl = history.index(history.getPosition() - i - lag);
            double yn = history.get(n) - average.get(n);
            double ynl = history.get(nl) - average.get(nl);

            sum += yn * ynl;
        }
        return sum;
    }

    /*
    public double average() {
        double total = 0;
        for (int i = 0; i < history.length; i++) {
            total += history[i];
        }
        return total / history.length;
    }

    public int index(int i) {
        int ind = i;
        if (ind < 0) {
            ind += history.length;
        }
        return ind % history.length;
    }
     */
}
