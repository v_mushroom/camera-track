/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.cameratrack;

/**
 *
 * @author mush
 */
public class Sequence {

    private final double[] sequence;
    private int position;
    public final int length;

    public Sequence(int len) {
        this.length = len;
        sequence = new double[length];
        clear();
    }

    public void clear() {
        for (int i = 0; i < length; i++) {
            sequence[i] = 0;
        }
        position = 0;
    }

    public void add(double value) {
        sequence[position] = value;

        position = index(position + 1);
    }

    public int getPosition() {
        return position;
    }

    public double get(int i) {
        return sequence[index(i)];
    }

    public void set(int i, double value) {
        sequence[index(i)] = value;
    }

    public double average() {
        double total = 0;
        for (int i = 0; i < length; i++) {
            total += sequence[i];
        }
        return total / length;
    }

    public int index(int i) {
        int ind = i;
        if (ind < 0) {
            ind += length;
        }
        return ind % length;
    }

}
