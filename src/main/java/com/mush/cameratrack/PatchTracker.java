/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.cameratrack;

import java.awt.geom.Rectangle2D;

/**
 *
 * @author mush
 */
public class PatchTracker {

    private Rectangle2D last;
    private Rectangle2D next;
    private Rectangle2D current;
    private double confidence;

    public PatchTracker() {
        last = new Rectangle2D.Double();
        current = new Rectangle2D.Double(0.5, 0.5, 0, 0);
        next = new Rectangle2D.Double();
        confidence = 0;
    }

    public Rectangle2D getCurrent() {
        return current;
    }

    public double getConfidence() {
        return confidence;
    }

    public void track(Rectangle2D patch) {
        if (patch == null) {
            confidence = 0;
            return;
        }

        last.setRect(next);
        next.setRect(patch);

        double comparison = compare(next, last);
        if (comparison < 0.01) {
            confidence = 0;
        } else {
            final double f = 0.3;
//            confidence = (1 - f) * confidence + f * comparison;
            confidence += (confidence + 0.01) * f * comparison;
            if (confidence < 0) {
                confidence = 0;
            }
            if (confidence > 1) {
                confidence = 1;
            }
        }

        updateCurrent();
    }

    private void updateCurrent() {
        double c = confidence;
        /*if (c > 0.5) {
            c = 1;
        } else {
            c *= 2;
            c *= c;
        }*/
//        c *= c;
        double notC = 1.0 - c;
        current.setRect(
                c * next.getX() + notC * current.getX(),
                c * next.getY() + notC * current.getY(),
                c * next.getWidth() + notC * current.getWidth(),
                c * next.getHeight() + notC * current.getHeight()
        );
    }

    private double compare(Rectangle2D rectA, Rectangle2D rectB) {
        Rectangle2D intersection = rectA.createIntersection(rectB);

        double intersectionArea = area(intersection);
        double unionArea = area(rectA) + area(rectB) - intersectionArea;

        if (unionArea < 0.001) {
            return 0;
        }

        return intersectionArea / unionArea;
    }

    private double area(Rectangle2D rect) {
        return rect.getWidth() * rect.getHeight();
    }

}
